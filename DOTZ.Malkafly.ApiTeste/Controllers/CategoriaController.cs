﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOTZ.Malkafly.ApiTeste.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DOTZ.Malkafly.ApiTeste.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class CategoriaController : Controller
    {
        IMongoCollection<CategoriaModel> collection;

        public CategoriaController(MongoClient client){
            var database = client.GetDatabase("dotz_testeapi");
            collection = database.GetCollection<CategoriaModel>("categoria");
        }

		[HttpGet]
        public async Task<JsonResult> Get()
		{
			var categorias = new List<CategoriaModel>();
            var todasCategorias = await collection.FindAsync(new BsonDocument());
			await todasCategorias.ForEachAsync(doc => categorias.Add(doc));

            return Json(categorias);
        }

		// POST api/values
		[HttpPost]
        public async Task Post([FromBody]CategoriaModel data)
		{
            await collection.InsertOneAsync(data);
		}
    }
}
