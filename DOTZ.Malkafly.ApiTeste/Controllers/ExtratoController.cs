﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DOTZ.Malkafly.ApiTeste.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DOTZ.Malkafly.ApiTeste.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ExtratoController : Controller
    {
        IMongoCollection<ExtratoModel> collection;

        public ExtratoController(MongoClient client){
            var database = client.GetDatabase("dotz_testeapi");
            collection = database.GetCollection<ExtratoModel>("extrato");
        }

		[HttpGet]
		public async Task<JsonResult> Get()
		{
            var username = HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
			var produtos = new List<ExtratoModel>();
            var query = collection.AsQueryable()
                                  .Where(p => p.Usuario.Equals(username)).ToList();

			return Json(query);
		}

	}
}
