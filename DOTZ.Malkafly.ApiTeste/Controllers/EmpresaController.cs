﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOTZ.Malkafly.ApiTeste.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DOTZ.Malkafly.ApiTeste.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class EmpresaController : Controller
    {
        IMongoCollection<EmpresaModel> collection;

        public EmpresaController(MongoClient client){
            var database = client.GetDatabase("dotz_testeapi");
            collection = database.GetCollection<EmpresaModel>("empresa");
        }

		[HttpGet]
        public async Task<JsonResult> Get()
		{
			var empresas = new List<EmpresaModel>();
            var todasEmpresas = await collection.FindAsync(new BsonDocument());
			await todasEmpresas.ForEachAsync(doc => empresas.Add(doc));

            return Json(empresas);
        }
	}
}
