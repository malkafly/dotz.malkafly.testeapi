﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DOTZ.Malkafly.ApiTeste.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DOTZ.Malkafly.ApiTeste.Controllers
{
    [Route("api/[controller]")]
    public class UsuarioController : Controller
    {
        IMongoCollection<UsuarioModel> collection;
        IMongoCollection<SaldoModel> collectionSaldo;

        public UsuarioController(MongoClient client){
            var database = client.GetDatabase("dotz_testeapi");
            collection = database.GetCollection<UsuarioModel>("usuario");
            collectionSaldo = database.GetCollection<SaldoModel>("saldo");
        }

		[HttpGet]
		[Authorize]
        public JsonResult Get()
		{
			var username = HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
			var query = collection.AsQueryable()
								  .Where(p => p.Email.Equals(username)).FirstOrDefault();

			return Json(query);
        }

		[HttpGet]
		public JsonResult Saldo()
		{
			var username = HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
			var query = collectionSaldo.AsQueryable()
								  .Where(p => p.Usuario.Equals(username))
                                       .Select(e => e.ValorPonto).Sum();

			return Json(query);
		}


		// POST api/values
		[HttpPost]
        public async Task Post([FromBody]UsuarioModel data)
		{
            await collection.InsertOneAsync(data);
		}
    }
}
