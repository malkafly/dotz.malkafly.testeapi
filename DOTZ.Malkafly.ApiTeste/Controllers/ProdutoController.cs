﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOTZ.Malkafly.ApiTeste.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DOTZ.Malkafly.ApiTeste.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ProdutoController : Controller
    {
        IMongoCollection<ProdutoModel> collection;

        public ProdutoController(MongoClient client){
            var database = client.GetDatabase("dotz_testeapi");
            collection = database.GetCollection<ProdutoModel>("produto");
        }

		[HttpGet]
        public async Task<JsonResult> Get()
		{
			var produtos = new List<ProdutoModel>();
            var todosProdutos = await collection.FindAsync(new BsonDocument());
			await todosProdutos.ForEachAsync(doc => produtos.Add(doc));

            return Json(produtos);
        }


		[HttpGet("{categoria}")]
		public JsonResult Get(string categoria)
		{
			var produtos = new List<ProdutoModel>();
            var filter = Builders<BsonDocument>.Filter.AnyEq("Categoria",categoria);
            var query = collection.AsQueryable()
                                  .Where(p => p.Categoria.Equals(categoria)).ToList();
			//var todosProdutos = await collection.FindAsync(filter);
			//await todosProdutos.ForEachAsync(doc => produtos.Add(doc));

			return Json(query);
		}

	}
}
