﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DOTZ.Malkafly.ApiTeste.Models
{
    public class CategoriaModel
    {
		[BsonId]
		public ObjectId CategoriaID
		{
			get;
			set;
		}

		[BsonRequired]
		public string Titulo
		{
			get;
			set;
		}

        public int CategoriaMaeID
        {
            get;
            set;
        }
    }
}
