﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DOTZ.Malkafly.ApiTeste.Models
{
    public class UsuarioModel
    {
		[BsonId]
		public ObjectId UsuarioID
		{
			get;
			set;
		}

		[BsonRequired]
		public string Email
		{
			get;
			set;
		}

		[BsonRequired]
		public string Senha
		{
			get;
			set;
		}

		[BsonRequired]
		public string Nome
		{
			get;
			set;
		}

        public string Sobrenome
		{
			get;
			set;
		}


		public string Empresa
		{
			get;
			set;
		}


	}
}
