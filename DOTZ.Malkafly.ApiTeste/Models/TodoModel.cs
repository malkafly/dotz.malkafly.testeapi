﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DOTZ.Malkafly.ApiTeste.Models
{
    public class TodoModel
    {
        [BsonId]
        public ObjectId TodoID
        {
            get;
            set;
        }

        [BsonRequired]
        public string Title
        {
            get;
            set;
        }

        public bool Completed
        {
            get;
            set;
        } = false;

        public DateTime? OptionalLine
        {
            get;
            set;
        } = null;
    }
}
