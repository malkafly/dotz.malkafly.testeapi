﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DOTZ.Malkafly.ApiTeste.Models
{
    public class EmpresaModel
    {
		[BsonId]
		public ObjectId EmpresaID
		{
			get;
			set;
		}

		public string Responsavel
		{
			get;
			set;
		}
		public string Empresa
		{
			get;
			set;
		}
		public string Endereco
		{
			get;
			set;
		}
        public string Telefone
		{
			get;
			set;
		}
		public string Email
		{
			get;
			set;
		}
    }
}
