﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DOTZ.Malkafly.ApiTeste.Models
{
    public class ExtratoModel
    {
		[BsonId]
		public ObjectId ExtratoID
		{
			get;
			set;
		}

        /// <summary>
        /// Proprietário do ponto
        /// </summary>
		public string Usuario
		{
			get;
			set;
		}

		public int ValorPonto
		{
			get;
			set;
		}

        /// <summary>
        /// Tipo do ponto, 1 = Entrada, 2 = Saída, 3 = Provisionado
        /// </summary>
        /// <value>The tipo ponto.</value>
		public int TipoPonto
		{
			get;
			set;
		}

		public string DataOperacao
		{
			get;
			set;
		}

        /// <summary>
        /// Para aonde o ponto foi
        /// </summary>
        /// <value>The destino.</value>
        public string Destino
        {
            get;
            set;
        }

        /// <summary>
        /// De onde o ponto veio
        /// </summary>
		public string Origem
		{
			get;
			set;
		}
	}
}
