﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DOTZ.Malkafly.ApiTeste.Models
{
    public class SaldoModel
    {
		[BsonId]
		public ObjectId SaldoID
		{
			get;
			set;
		}

		/// <summary>
		/// Proprietário do ponto
		/// </summary>
		public string Usuario
		{
			get;
			set;
		}

        /// <summary>
        /// Valor final do Saldo, incrementado e decrementado de acordo com as
        /// operações no extrato
        /// </summary>
		public int ValorPonto
		{
			get;
			set;
		}
    }
}
