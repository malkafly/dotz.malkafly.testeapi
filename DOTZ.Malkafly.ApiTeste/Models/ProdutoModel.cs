﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DOTZ.Malkafly.ApiTeste.Models
{
    public class ProdutoModel
    {
		[BsonId]
		public ObjectId ProdutoID
		{
			get;
			set;
		}

		[BsonRequired]
		public string Titulo
		{
			get;
			set;
		}

		public string Descricao
		{
			get;
			set;
		}

		public int ValorPontos
		{
			get;
			set;
		}

		public string Categoria
		{
			get;
			set;
		}
    }
}
